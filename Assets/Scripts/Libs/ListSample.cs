﻿using System;
using System.Collections;
using System.Collections.Generic;

public class ListSample{
	static Random rnd = new Random();
	public static T PickOne<T>(List<T> list){
		int i = rnd.Next (list.Count);
		return list [i];
	}

	public static T PickOneByDist<T>(List<T> list, float distibution){
		if (distibution < 50.0F) {
			throw new Exception ("distribution should be bigger than 5.0");
		}
		float n = (new FloatRange(0.0F, 100.0F)).PickRandom();
		float currentD = distibution;
		int i = 0;

		while (currentD < n && i < (list.Count - 1)) {
			i++;
			currentD = (currentD + 100.0F) / (100.0F / distibution);
		}

		return list [i];
	}
}