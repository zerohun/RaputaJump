﻿/*

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class StageGenerator {

	static void GenerateTransforms(ref List<CustomTransform> positionRotations, Vector3 from, Vector3 current, float maxRadius, float minRadius, float maxRadiusHeight, float minRadiusHeight, float maxGap, float minGap, int recusiveCount){

		if (minRadius < 0.0F || maxRadius < 0.0F) {
			throw new System.ArgumentException ("Radius can not be negative");
		}

		if (minGap < 0.0F || maxGap < 0.0F) {
			throw new System.ArgumentException ("Gap can not be negative");
		}
		
		Debug.Log (from);
		Debug.Log(current);
		Debug.Log("---------------");

		float angle = Random.Range (0.0F, Mathf.PI);
		float newAnagle = Mathf.Atan2 (current.z - from.z, current.x - from.x) + angle - Mathf.PI / 2.0F;
		float radius = Random.Range (minRadius, maxRadius);
		float newX = Mathf.Cos (newAnagle) * radius + current.x;
		float newY = Random.Range (minRadiusHeight, maxRadiusHeight) + current.y;
		float newZ = Mathf.Sin (newAnagle) * radius + current.z;

		float stairX, stairZ;
		float sumOfDistance = 0.0F;

		List<CustomTransform> localCTs = new List<CustomTransform>();

		while (sumOfDistance < radius - 20.0F) {
			sumOfDistance += minGap; //Random.Range (minGap, maxGap);
			stairX = sumOfDistance * Mathf.Cos (newAnagle) + current.x;
			stairZ = sumOfDistance * Mathf.Sin (newAnagle) + current.z;
			CustomTransform ct = new CustomTransform ();
			ct.position = new Vector3 (stairX, 0.0F, stairZ);
			ct.rotation = Quaternion.AngleAxis (-newAnagle * Mathf.Rad2Deg, Vector3.up);
			localCTs.Add (ct);
		}

		float dy = newY - current.y;

		for (int i = 0; i < localCTs.Count; i++) {
			Vector3 position = localCTs [i].position;
			float stairY = current.y + dy / localCTs.Count * i;

			CustomTransform ct = new CustomTransform ();
			ct.position = new Vector3 (position.x, stairY, position.z);
			ct.rotation = localCTs [i].rotation;
			localCTs [i] = ct;
		}
			
		positionRotations.AddRange (localCTs);	

		if (recusiveCount > 1) {
			GenerateTransforms (ref positionRotations, current, new Vector3(newX, newY, newZ), maxRadius, minRadius, maxRadiusHeight, minRadiusHeight, maxGap, minGap, recusiveCount - 1);
		}
	}
}

*/
