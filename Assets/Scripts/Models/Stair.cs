﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stair : Ground {
	public Stair (GameObject gameObject, CustomTransform ct) : base (gameObject, ct) {
	}
	public override string GroundType {
		get{ return "Ground"; }
	}
}
