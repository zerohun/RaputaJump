﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

interface IRenderable {
	void GetRenderableGrounds(ref List<Ground> grounds);
}