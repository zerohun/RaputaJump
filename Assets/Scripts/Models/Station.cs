﻿using UnityEngine;
using System.Collections;

public class Station : Ground {
	public Station (GameObject gameObject, CustomTransform ct) : base (gameObject, ct) {}
	public override string GroundType {
		get{ return "Station"; }
	}
}
