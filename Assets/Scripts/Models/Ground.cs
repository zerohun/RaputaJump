﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Ground {
	public CustomTransform transform;
	public GameObject gameObject;

	public Ground() {}
	public Ground(GameObject gameObject, CustomTransform ct){
		this.gameObject = gameObject;	
		this.transform = ct;
	}
	public abstract string GroundType { get;}
}
