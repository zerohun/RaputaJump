﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Route : IRenderable {
	float GAP_ABERRATION = 20.0F;

	public Vector3 from;
	public Vector3 to;
	public FloatRange gap;
	public List<Stair> stairs;
	public GameObject stairGameObject;

	private List<CustomTransform> cts;
	private Vector3Range stairSize;
	private float stairYGap;

	public Route(Vector3 from, Vector3 to, float maxJumpHeight, float fowardPowerRatio, Vector3Range stairSize, List<GameObject> stairGameObjs, float stairDistribution, GameObject elevatorStairObj){
		this.from = from;
		this.to = to;

		float forwardJumpPower = maxJumpHeight * fowardPowerRatio / 100;
		this.gap = new FloatRange (forwardJumpPower - forwardJumpPower * GAP_ABERRATION / 100,
			forwardJumpPower);

		float xzDistance = Vector2.Distance(new Vector2(to.x, to.z),
		       							new Vector2(from.x, from.z));
		float sumOfGaps = 0.0F;
		float xzAngle = Mathf.Atan2 (to.z - from.z, to.x - from.x);
		Vector3 pickedStairSize;
		stairs = new List<Stair> ();
		cts = new List<CustomTransform> ();

		while (sumOfGaps < xzDistance) {
			pickedStairSize = stairSize.PickRandom ();			
			sumOfGaps += (gap.PickRandom () + pickedStairSize.x);

			CustomTransform ct = new CustomTransform ();
			ct.position = new Vector3 (sumOfGaps * Mathf.Cos (xzAngle) + from.x,
				0.0F,
				sumOfGaps * Mathf.Sin (xzAngle) + from.z);
			ct.scale = pickedStairSize;
			ct.rotation = Quaternion.AngleAxis (-xzAngle * Mathf.Rad2Deg, Vector3.up);
			cts.Add (ct);
		}

		int i;
		stairYGap = (to.y - from.y) / cts.Count;
		float yStairStart = 0.0F;

		if (stairYGap > maxJumpHeight) {
			CustomTransform ct = cts [0];	
			ct.position.y = from.y - ct.scale.y / 2.0F;
			float gapToFill = (to.y - from.y) - (maxJumpHeight * cts.Count);
			(elevatorStairObj.GetComponent<ElevatorController> ()).moveTo = new Vector3 (ct.position.x, ct.position.y + gapToFill, ct.position.z);
			stairs.Add (new Stair (elevatorStairObj, ct));
			stairYGap = maxJumpHeight;
			i = 1;
			yStairStart = gapToFill;
		} else {
			i = 0;
			yStairStart = 0.0F;
		}


		for (; i < cts.Count; i++) {
			CustomTransform ct = cts [i];	
			ct.position.y = yStairStart + from.y + stairYGap * i - ct.scale.y / 2.0F;
			GameObject stairGameObj = ListSample.PickOneByDist<GameObject> (stairGameObjs, stairDistribution);
			stairs.Add (new Stair (stairGameObj, ct));
		}
	}

	public void GetRenderableGrounds(ref List<Ground> grounds){
		for (int i = 0; i < stairs.Count; i++) {
			grounds.Add (stairs[i]);
		}
	}
}
