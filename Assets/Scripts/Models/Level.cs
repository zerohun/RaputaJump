﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level {
	float START_ROUTE_LENGTH = 300.0F;
	float MIN_ROUTE_LENGTH = 200.0F;
	float ABERRATION_RATIO_ROUTE_LENGTH = 20.0F;
	float DIFFICULTIATOR_ROUTE_LENGTH = 10.0F;

	float START_JUMP_POWER = 5.0F;
	float MAX_JUMP_POWER = 20.0F;
	float DIFFICULTIATOR_JUMP_POWER = 1.5F;

	int START_TOTAL_ROUTE = 2;
	int MAX_TOTAL_ROUTE = 100;
	int DIFFICULTIATOR_TOTAL_ROUTE = 10;

	float START_HEIGHT = 200.0F;
	float MAX_HEIGHT = 500.0F;
	float DIFFICULTIATOR_MAX_HEIGHT = 10.0F;

	float START_BRICK_DISTIBUTION = 100.0F;
	float MIN_BRICK_DISTIBUTION = 50.0F;
	float DIFFICULTIATOR_BRICK_DISTIBUTION = 5.0F;

	Vector3 START_STAIR_SIZE = new Vector3 (10.0F, 10.0F, 10.0F);
	Vector3 MIN_STAIR_SIZE = new Vector3 (1.0F, 5.0F, 1.0F);
	Vector3 DIFFICULTIATOR_STAIR_SIZE = new Vector3 (5.0F, 0.0F, 5.0F);
	float ABERRATION_RATIO_STAIR_SIZE_RATIO = 20.0F;

	private Station station;
	private int level;
	private Vector3 from;
	private Vector3 to;
	private Level lastLevel;

	private Route lastRouteAdded = null;

	private int totalRoute;
	private List<Route> routes;

	private List<GameObject> stairGameObjs;
	private List<GameObject> stationGameObjs;
	private GameObject elevatorStairObj;

	private float brickDistribution;

	public Level(int level, Level lastLevel,  List<GameObject> stairGameObjs, List<GameObject> stationGameObjs, GameObject elevatorStairObj){
		this.level = level;
		this.lastLevel = lastLevel;
		this.from = (lastLevel == null) ? Vector3.zero:lastLevel.To;
		this.totalRoute = Mathf.Min(MAX_TOTAL_ROUTE, START_TOTAL_ROUTE + (this.level - 1) * DIFFICULTIATOR_TOTAL_ROUTE);
		this.station = null;
		routes = new List<Route> ();

		this.stairGameObjs = stairGameObjs;
		this.stationGameObjs = stationGameObjs;

		this.elevatorStairObj = elevatorStairObj;

		brickDistribution = START_BRICK_DISTIBUTION - (level - 1) * DIFFICULTIATOR_BRICK_DISTIBUTION;
		brickDistribution = Mathf.Max (MIN_BRICK_DISTIBUTION, brickDistribution);
	}

	public Vector3 From { get { return from; } }
	public Vector3 To { get { return to; } }
	public int LevelNumber { get { return level; } }

	public void Generate(out List<Ground> grounds){
		grounds = new List<Ground> ();
		//Debug.Log ("Generate run");
		if (this.IsCompleted == false) {
			//Debug.Log ("attemp to build ground");

			Vector3Range stairSize = GetStairSize ();
			float maxJumpHeight = GetJumpPower ();

			if (level == 1 && this.routes.Count == 0) {
				Route firstRoute = new Route (this.from, new Vector3(0.0F, 10.0F, 300.0F), maxJumpHeight, 40.0F, stairSize, this.stairGameObjs, brickDistribution, elevatorStairObj);
				lastRouteAdded = firstRoute;
				this.routes.Add (firstRoute);
				firstRoute.GetRenderableGrounds (ref grounds);
			}

			Vector3 routeTo, routeFrom;
			if (lastRouteAdded == null) {
				routeFrom = from;
			} else {
				routeFrom = lastRouteAdded.to;
			}
			
			routeTo = PickDestination (routeFrom, this.from);

			Route route = new Route (routeFrom, routeTo, maxJumpHeight, 40.0F, stairSize, this.stairGameObjs, brickDistribution, elevatorStairObj);
			this.routes.Add (route);
			route.GetRenderableGrounds (ref grounds);

			lastRouteAdded = route;

			if (this.routes.Count == totalRoute) {
				GameObject stationGameObj = ListSample.PickOne<GameObject> (this.stationGameObjs);
				TextMesh levelTextMesh = stationGameObj.GetComponentInChildren<TextMesh> ();
				levelTextMesh.text = "Level " + (this.level + 1);
				Vector3 stationBoxSize = (stationGameObj.GetComponentInChildren<MeshCollider> ()).sharedMesh.bounds.size;
				CustomTransform ct = new CustomTransform ();
				ct.position = new Vector3 (stationBoxSize.x / 3, -1.0F, stationBoxSize.z / 3) + 2 * lastRouteAdded.stairs [lastRouteAdded.stairs.Count - 1].transform.position - lastRouteAdded.stairs [lastRouteAdded.stairs.Count - 2].transform.position;
				//ct.position = new Vector3 (lastRouteAdded.to.x + stationBoxSize.x / 2 + 5.0F, lastRouteAdded.to.y - 0.5F, lastRouteAdded.to.z + stationBoxSize.z / 2 + 5.0F);
				ct.scale = new Vector3 (1.0F, 1.0F, 1.0F);
				ct.rotation = lastRouteAdded.stairs[lastRouteAdded.stairs.Count - 1].transform.rotation;
				this.station = new Station (stationGameObj, ct);
				grounds.Add (station);
				this.to = ct.position + new Vector3 (stationBoxSize.x / 3, 0.0F, stationBoxSize.z / 3);
			}
		}
	}

	public bool IsCompleted { 
		get { 
			//Debug.Log (totalRoute + " vs " + this.routes.Count);
			return this.totalRoute == this.routes.Count && this.station != null;
		} 
	}

	private Vector3 PickDestination(Vector3 from, Vector3 lastFrom) {
		float angle = Random.Range (0.0F, Mathf.PI);
		float newAnagle = Mathf.Atan2 (from.z - lastFrom.z, from.x - lastFrom.x) + angle - Mathf.PI / 2.0F;
		float maxRadius = Mathf.Max(MIN_ROUTE_LENGTH, START_ROUTE_LENGTH - this.level * DIFFICULTIATOR_ROUTE_LENGTH);
		float minRadius = maxRadius - maxRadius / 100.0F * ABERRATION_RATIO_ROUTE_LENGTH;
		float radius = Random.Range (minRadius, maxRadius);

		return new Vector3 (Mathf.Cos (newAnagle) * radius + from.x,
			GetHeight () + from.y,
			Mathf.Sin (newAnagle) * radius + from.z);
	}

	private float GetJumpPower() {
		return Mathf.Min(START_JUMP_POWER + DIFFICULTIATOR_JUMP_POWER * (this.level - 1), MAX_JUMP_POWER);
	}

	private float GetHeight() {
		float maxHeight = Mathf.Min(MAX_HEIGHT, START_HEIGHT + DIFFICULTIATOR_MAX_HEIGHT * (this.level - 1));
		float minHeight = -maxHeight;
		return Random.Range (minHeight, maxHeight);
	}

	private Vector3Range GetStairSize(){
		Vector3 maxSize = Vector3.Max (MIN_STAIR_SIZE, START_STAIR_SIZE - DIFFICULTIATOR_STAIR_SIZE * (this.level - 1));
		Vector3 minSize = maxSize * ABERRATION_RATIO_STAIR_SIZE_RATIO / 100.0F;
		return new Vector3Range (minSize, maxSize);
	}
}
