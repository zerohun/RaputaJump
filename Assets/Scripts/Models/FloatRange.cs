﻿using UnityEngine;
using System.Collections;

public class FloatRange {
	public float min;
	public float max;

	public FloatRange(float min, float max){
		this.min = min;
		this.max = max;
	}

	public float PickRandom(){
		return Random.Range (this.min, this.max);
	}
}
