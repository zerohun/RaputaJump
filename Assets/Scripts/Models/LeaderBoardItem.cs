﻿using System;

public class LeaderBoardItem {
	public String playerName;
	public int playerRank;
	public long playerScore;

	override public String ToString(){
		return playerRank + "                " + playerScore + "                " + playerName;
	}
}