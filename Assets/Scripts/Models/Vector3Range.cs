﻿using UnityEngine;
using System.Collections;

public class Vector3Range {
	public Vector3 min;
	public Vector3 max;

	public Vector3Range (Vector3 min, Vector3 max) {
		this.min = min;
		this.max = max;
	}

	public Vector3 PickRandom() {
		return new Vector3 (Random.Range (min.x, max.x),
			Random.Range (min.y, max.y),
			Random.Range (min.z, max.z));
	}
}
