﻿using System;
using UnityEngine;

public class CustomTransform {
	public Vector3 position;
	public Quaternion rotation;
	public Vector3 scale;
}