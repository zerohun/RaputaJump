﻿using UnityEngine;
using System.Collections;

public abstract class MovingStairController : MonoBehaviour {
	// Update is called once per frame
	float DISTANCE_THRESHOLD = 100.0F;

	public GameObject playerObj;

	protected bool isMoving = false;
	protected bool isMovingPaused = false;

	private Vector3 playerPosition;
	private Vector3 currentPosition;
	private float distance;

	void Start(){
		currentPosition = gameObject.transform.position;
		Init ();
	}

	void Update(){
		if (!isMovingPaused) {
			playerPosition = playerObj.transform.position;
			distance = Vector3.Distance (playerPosition, currentPosition);
			if (!isMoving) {
				if (distance < DISTANCE_THRESHOLD) {
					StartCoroutine ("Move");
					isMoving = true;
				}
			}
		}
	}

	protected abstract void Init();
}
