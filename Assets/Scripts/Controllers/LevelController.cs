﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour {
	public GameObject playerObj;
	public List<GameObject> stairGameObjs;
	public List<GameObject> stationGameObjs;
	public GameObject elevatorStairObj;
	public GameObject levelTextObj;
	public GameController gameController;

	private Level currentLevel = null;
	private Vector2 currentPosition;
	private Vector2 targetPosition;
	private float distance;
	private Vector3 playerPosition;
	private List<Ground> groundsToRender;
	private bool isGenerating = true;

	void Start(){
		for (int i = 0; i < this.stairGameObjs.Count; i++) {
			GameObject stair = this.stairGameObjs [i];
			OnContactController ocController = stair.GetComponent<OnContactController> ();
			ocController.gameController = gameController;
		}
	}

	public void StopGeneratingLoop() {
		isGenerating = false;
	}

	public IEnumerator ResetingLoop(){
		yield return StartCoroutine (gameController.StartLoading ());
		GameObject [] groundObjects = GameObject.FindGameObjectsWithTag ("Ground");
		for (int i = 0; i < groundObjects.Length; i++) {
			Destroy (groundObjects [i]);
//			Debug.Log ("Destroy Object");
			if (i % 100 == 0) {
				yield return null;
			}
		}
		currentLevel = new Level (1, null, stairGameObjs, stationGameObjs, elevatorStairObj);
		isGenerating = true;
		yield return null;
	}
		
	public IEnumerator GeneratingLoop () {
		while(isGenerating){
//			Debug.Log ("GeneratingLoop");
			if (currentLevel == null) {
				currentLevel = new Level (1, null, stairGameObjs, stationGameObjs, elevatorStairObj);
			}
			if (currentLevel.IsCompleted) {
//				Debug.Log ("Complete!! creating new level");
				currentLevel = new Level (currentLevel.LevelNumber + 1, currentLevel, stairGameObjs, stationGameObjs, elevatorStairObj);
				RenderGrounds ();

			} else {
//				Debug.Log ("NOT Complete!!");
				playerPosition = playerObj.transform.position;
				currentPosition = new Vector2 (playerPosition.x, playerPosition.z);
				targetPosition = new Vector2 (currentLevel.To.x, currentLevel.To.z);
				distance = Vector2.Distance (currentPosition, targetPosition);
//				Debug.Log ("Distance: " + distance + "  player: " + currentPosition + "   targetPosition:" + targetPosition);
				if (distance < 500.0f) {
					RenderGrounds ();
				}
			}
			yield return null;
		}
		yield return null;
	}

	void RenderGrounds(){
//		Debug.Log ("in the distance.... process");	
		currentLevel.Generate (out groundsToRender);
		for (int i = 0; i < groundsToRender.Count; i++) {
			Ground ground = groundsToRender [i];
			ground.gameObject.transform.localScale = ground.transform.scale;
			Instantiate (ground.gameObject, ground.transform.position, ground.transform.rotation);
		}
	}
}
