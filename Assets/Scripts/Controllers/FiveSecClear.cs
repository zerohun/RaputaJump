﻿using UnityEngine;
using System.Collections;
using System;

public class FiveSecClear : MonoBehaviour {

	public GameObject playerObject;
	private DateTime startedAt;

	// Use this for initialization
	void Start () {
		startedAt = DateTime.Now;
	}
	
	// Update is called once per frame
	void Update () {
		if ((DateTime.Now - startedAt).TotalMilliseconds >= 2000) {
			playerObject.transform.position = new Vector3 (0.0F, 0.0F, 30.0F);
		}
	}
}