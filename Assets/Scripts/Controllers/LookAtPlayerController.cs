﻿using UnityEngine;
using System.Collections;

public class LookAtPlayerController : MonoBehaviour {

	public GameObject playerObj;
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.LookAt (playerObj.transform);
	}
}
