﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using VRStandardAssets.Utils;
using Oculus.Platform;
using Oculus.Platform.Models;


public class GameController : MonoBehaviour {

	public GameObject playerObj;
	public GameObject loadingSquare;
	public LevelController levelController;
	public ScoreCanvasController scoreCanvasController;
	public Canvas introCanvas;
	public Canvas outroCanvas;

	private int totalSteppedStairs = 0;
	private List<LeaderBoardItem> leaderBoardItems = new List<LeaderBoardItem>();
	private LeaderBoardItem leaderBoardItem;
	private String userName;

	void Start() {
		Core.Initialize("1363007320396016");
		Entitlements.IsUserEntitledToApplication().OnComplete(
			(Message msg) =>
			{
				if (msg.IsError)
				{
					// User is NOT entitled.
					Debug.Log("User is NOT entitled.");
				}  else 
				{
					StartCoroutine(GameLoop());
				}
			}
		);
	}

	IEnumerator GameLoop() {
		while (true) {
			yield return StartCoroutine (levelController.GeneratingLoop ());
			yield return StartCoroutine (StartLoading ());
			yield return StartCoroutine (levelController.ResetingLoop ());
			yield return StartCoroutine (FinishLoading ());
			Reset ();
		}
	}

	public void OnGameOver(){
		introCanvas.gameObject.SetActive (false);
		outroCanvas.gameObject.SetActive (true);

		WriteAndLoadLeaderBoard (3);
		totalSteppedStairs = 0;
		levelController.StopGeneratingLoop ();
	}

	public void Reset() {
		Debug.Log ("Reset");
		Debug.Log (this.totalSteppedStairs);	
		playerObj.transform.rotation = Quaternion.identity;
		playerObj.transform.position = new Vector3 (0.0F, 50.0F, 0.0F);
	}

	public void OnSteppedStair() {
		this.totalSteppedStairs = this.totalSteppedStairs + 1;
		Debug.Log (this.totalSteppedStairs);
	}

	public IEnumerator StartLoading() {
		loadingSquare.SetActive (true);
		yield return null;
	}

	public IEnumerator FinishLoading() {
		loadingSquare.SetActive (false);
		yield return null;
	}

	private void WriteAndLoadLeaderBoard(int totalAttempts){
		if (totalAttempts == 0) {
			return;
		}

		Leaderboards.WriteEntry("scoreboard", totalSteppedStairs, null, false).OnComplete(
			(Message msg) => {
				if (msg.IsError) {
					// Network error or something.
					Debug.Log ("write error");
					WriteAndLoadLeaderBoard(totalAttempts - 1);
				}
				else {
					Leaderboards.GetEntries ("scoreboard", 10000, LeaderboardFilterType.None, LeaderboardStartAt.Top).OnComplete (
						(Message<LeaderboardEntryList> msg2) => {
							if (msg.IsError) {
								// Network error or something.
								Debug.Log("Leader board error: " + msg2.GetError().Message);
								WriteAndLoadLeaderBoard(totalAttempts - 1);
								return;
							}  

							Users.GetLoggedInUser().OnComplete(
								(Message<User> userMsg) => {
									if (userMsg.IsError) {
										// Network error or something.
										Debug.Log("user error: " + userMsg.GetError().Message);
										WriteAndLoadLeaderBoard(totalAttempts - 1);
										return;
									}  

									userName = userMsg.GetUser().OculusID;
									Debug.Log("user Name: " + userName);
									LeaderboardEntryList list = msg2.GetLeaderboardEntryList();
									for(int i = 0; i < list.Count; i++) {
										if(i < 10) {
											LeaderBoardItem item = new LeaderBoardItem();
											item.playerName = list[i].User.OculusID;
											item.playerRank = list[i].Rank;
											item.playerScore = list[i].Score;
											leaderBoardItems.Add(item);
										}

										if(list[i].User.OculusID == userName){
											leaderBoardItem = new LeaderBoardItem();
											leaderBoardItem.playerName = list[i].User.OculusID;
											leaderBoardItem.playerRank = list[i].Rank;
											leaderBoardItem.playerScore = list[i].Score;
										}
									}
										
									scoreCanvasController.SetLeaderBoard(leaderBoardItems, leaderBoardItem);
								}
							);
						}
					);
				}   
			}
		);
	}
}
