﻿using UnityEngine;
using System;
using System.Collections;

public class OnReachGoal : MonoBehaviour {

	public GameObject windowObj;
	public WindowController windowController;

	private DateTime startedAt;

	// Use this for initialization
	void Start () {
		windowController = windowObj.GetComponent<WindowController> ();
		startedAt = DateTime.Now;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider other) {
		windowObj.SetActive (true);
		windowController.secSpent = (float) 	((DateTime.Now - startedAt).TotalMilliseconds / 1000.0F);
	}
}
