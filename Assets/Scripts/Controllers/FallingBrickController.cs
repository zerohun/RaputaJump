﻿using UnityEngine;
using System.Collections;

public class FallingBrickController : MovingStairController {	
	// Update is called once per frame
	float SHAKING_SCOPE = 1.0F;

	private float originalHeight;
	private float currentHeight;
	private bool isGoingUp = false;
	private float shakingSpeed = 2.0F;	

	protected override void Init(){
		originalHeight = gameObject.transform.position.y;
	}


	void OnCollisionEnter(Collision collision){
		StopCoroutine ("Move");
		this.isMovingPaused = true;
		StartCoroutine ("Fall");
	}

	IEnumerator Move() {
		while (true) {
			currentHeight = gameObject.transform.position.y;
			Debug.Log ("cur : " + currentHeight);
			Debug.Log ("ori : " + originalHeight);

			Debug.Log ("Shaking...");
			if (isGoingUp) {
				if (currentHeight > originalHeight + SHAKING_SCOPE) {
					isGoingUp = false;
					shakingSpeed = 0.1F;
					yield return new WaitForSeconds (1.0F);
				} else {
					Debug.Log ("Up");
					gameObject.transform.Translate (0.0F, shakingSpeed, 0.0F);
					shakingSpeed += 0.1F;
				}
			} else {
				if (currentHeight < originalHeight - SHAKING_SCOPE) {
					isGoingUp = true;
					shakingSpeed = 0.1F;

				} else {
					Debug.Log ("Down");
					shakingSpeed += 0.1F;
					gameObject.transform.Translate (0.0F, -shakingSpeed, 0.0F);
				}
			}
			yield return null;
		}
	}

	IEnumerator Fall(){
		yield return new WaitForSeconds (2.0F);
		gameObject.AddComponent<Rigidbody> ();
	}
}
