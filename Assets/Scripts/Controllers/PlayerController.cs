﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour {

	public GameController gameController;

	private Rigidbody rb;
	private DateTime startedJumpAt;
	// Use this for initialization
	void Start () {
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		if (rb.velocity.y < -200.0F) {
			rb.velocity = Vector3.zero;
			gameController.OnGameOver ();
		}
	}

	public void init(){
		rb = GetComponent<Rigidbody> ();
		startedJumpAt = DateTime.MaxValue;
	}

	public void jump(float jumpPower, Vector3 forwardVector){
		if (startedJumpAt == DateTime.MaxValue) {
			startedJumpAt = DateTime.Now;
		}

		if ((DateTime.Now - startedJumpAt).TotalMilliseconds < 500.0f) {
			rb.AddForce ((Vector3.up * jumpPower * 100) + (forwardVector * jumpPower * 40)); 
		}
	}

	void OnCollisionEnter(Collision collision) {
		startedJumpAt = DateTime.MaxValue;
	}
}
