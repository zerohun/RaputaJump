﻿	using UnityEngine;
using System.Collections;

public class ElevatorController : MovingStairController {
	public Vector3 moveTo;
	public float speed = 0.00001F;	

	private bool isGoingUp = true;
	private Vector3 originalPosition;
	private Vector3 eachMovement;
	private float pathLength;
	private float distanceToOrignal;
	private float distanceToTarget;

	protected override void Init () {
		originalPosition = gameObject.transform.position;
		pathLength = Vector3.Distance (originalPosition, moveTo);
		eachMovement = (moveTo - originalPosition)  / pathLength * speed;
	}
	
	// Update is called once per frame
	IEnumerator Move() {
		while (true) {
			distanceToTarget = Vector3.Distance (gameObject.gameObject.transform.position, moveTo);
			distanceToOrignal = Vector3.Distance (gameObject.gameObject.transform.position, originalPosition);

			if (isGoingUp) {
				Debug.Log ("going up");
				//Debug.Log (distance);
				if (distanceToTarget > 0.0F && distanceToOrignal < pathLength) {
					gameObject.transform.Translate (eachMovement);
				} else {
					isGoingUp = false;
				}
			} else {
				Debug.Log ("going down");
				//Debug.Log (distance);
				if (distanceToOrignal > 0.0F && distanceToTarget < pathLength) {
					gameObject.transform.Translate (-eachMovement);
				} else {
					isGoingUp = true;
				}
			}
			yield return null;
		}
	}
}
