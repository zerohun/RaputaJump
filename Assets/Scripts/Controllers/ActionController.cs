﻿using UnityEngine;
using UnityEngine.VR;
using System.Collections;
using System.Collections.Generic;


public class ActionController : MonoBehaviour {

	public GameObject playerGameObject;

	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public bool jumpEnabled = false;

	private float rotationX = 0.0F;
	private float rotationY = 0.0F;
	private float rotAverageX = 0.0F;	
	private float rotAverageY = 0.0F;

	private float minimumX = -360F;
	private float maximumX = 360F;

	private float minimumY = -60F;
	private float maximumY = 60F;
	private float jumpPower = 0.0F;

	private Quaternion originalRotation;
	private Vector3 forwardVector;

	private PlayerController playerController;

	// Use this for initialization
	void Start () {
		originalRotation = playerGameObject.transform.localRotation;
		Cursor.visible = false;
		Input.gyro.enabled = true;
		Cursor.lockState = CursorLockMode.Locked;
	}

	
	// Update is called once per frame
	void FixedUpdate () {

		if (!jumpEnabled) {
			if (!VRDevice.isPresent) {
				HandleMouseInput ();
			}
			return;
		}
		
		if (VRDevice.isPresent) {
			HandleMotionInput ();
		} else {
			HandleMouseInput ();
			HandleKeyboardInput ();
		}

		playerController = playerGameObject.GetComponent<PlayerController> ();
		//Debug.Log (jumpPower);
		if (jumpPower > 0.3F) {
			playerController.jump (jumpPower, forwardVector);
			jumpPower = 0.0F;
		}
	}

	private void HandleKeyboardInput(){
		forwardVector = playerGameObject.transform.localRotation * Vector3.forward;
		if (Input.GetKeyDown ("space")) {
			jumpPower = 5.0f;
		}
	}

	private void HandleMotionInput(){
		
		jumpPower = Input.gyro.userAcceleration.y;
	}

	private void HandleMouseInput(){
		forwardVector = InputTracking.GetLocalRotation (VRNode.CenterEye) * Vector3.forward;
		rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
		rotationX += Input.GetAxis("Mouse X") * sensitivityX;

		rotAverageY = ClampAngle (rotationY, minimumY, maximumY);
		rotAverageX = ClampAngle (rotationX, minimumX, maximumX);

		Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
		Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);
		playerGameObject.transform.localRotation = originalRotation * xQuaternion * yQuaternion;

		float translation = Input.GetAxis ("Vertical") * Time.deltaTime * 200.0F;
		//Debug.Log (translation);
		playerGameObject.transform.Translate (forwardVector * translation);
	}


	public static float ClampAngle (float angle, float min, float max)
	{
		angle = angle % 360;
		if ((angle >= -360F) && (angle <= 360F)) {
			if (angle < -360F) {
				angle += 360F;
			}
			if (angle > 360F) {
				angle -= 360F;
			}			
		}
		return Mathf.Clamp (angle, min, max);
	}
}
