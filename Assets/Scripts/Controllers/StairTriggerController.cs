﻿using UnityEngine;
using System.Collections;

public class StairTriggerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		BoxCollider triggerCollider = gameObject.AddComponent<BoxCollider> ();
		triggerCollider.isTrigger = true;
		triggerCollider.size = new Vector3 (1.0F, 6.0F, 4.0F);
		triggerCollider.center = new Vector3 (0.0F, 3.0F, 0.0F);
	}
}
