﻿using UnityEngine;
using System.Collections;

public class OnContactController : MonoBehaviour {

	public Material steppedMaterial;
	public GameController gameController;

	private MeshRenderer meshRenderer;


	private bool isStepped = false;

	void Start(){
		meshRenderer = gameObject.GetComponent<MeshRenderer> ();	
	}

	void OnTriggerEnter(Collider collider){
//		Debug.Log ("OnTriggerEnter");
		if (collider.gameObject.gameObject.name == "Player" && !isStepped) {
//			Debug.Log ("player contact");
			isStepped = true;
			meshRenderer.material = steppedMaterial;
			gameController.OnSteppedStair ();
		}
	}
}
