﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Oculus.Platform.Models;

public class ScoreCanvasController : MonoBehaviour {

	public Text leaderBoardUIText;
	public Text playerScoreText;

	private String leaderBoardText = "";

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
		
	public void SetLeaderBoard(List<LeaderBoardItem> leaderBoardItems, LeaderBoardItem userItem){
		
		for (int i = 0; i < leaderBoardItems.Count; i++) {
			leaderBoardText += leaderBoardItems[i].ToString ();
			leaderBoardText += "\n";
		}
		leaderBoardUIText.text = leaderBoardText;
		playerScoreText.text = userItem.ToString ();
	}
}
