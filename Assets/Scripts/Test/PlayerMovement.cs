﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public GameObject PlayerObject;

	private Vector3 forwardVector;
	private PlayerController playerController;
	private bool hasJumped;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		playerController = PlayerObject.GetComponent<PlayerController> ();
		forwardVector = new Vector3 (0.0f, 0.0f, 1.0f);
		hasJumped = false;
		rb = GameObject.Find ("Player").GetComponents<Rigidbody> () [0];
	}
	
	// Update is called once per frame
	void Update () {

		if (!hasJumped) {
			playerController.jump (5.0f, forwardVector);
			hasJumped = true;
		}
		Debug.Log (rb.velocity.y);
	}
}
